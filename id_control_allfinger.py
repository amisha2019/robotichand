import pybullet as bullet
import time
import numpy as np
import matplotlib.pyplot as plt
import math
import json
plot = True

verbose = False

# Parameters:
robot_base = [0., 0., 0.]
robot_orientation = [0., 0., 0., 1.]
delta_t = 0.01

# Initialize Bullet Simulator
id_simulator = bullet.connect(bullet.GUI)  # or bullet.DIRECT for non-graphical version
bullet.setTimeStep(delta_t)

#do not change id revolute joints, gave error
id_revolute_joints = [5,6,7,9,10,11,13,14,15,17,18,19,21,22,23]
id_robot = bullet.loadURDF("urdf/fingers.urdf",
                             robot_base,
                             robot_orientation,
                             globalScaling=0.50,
                             useFixedBase=True)

# Disable the motors for torque control:
bullet.setJointMotorControlArray(id_robot,
                                 id_revolute_joints,
                                 bullet.POSITION_CONTROL,
                                 forces = [0.0, 0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0,0.0, 0.0, 0.0, 0.0,0.0]) #do not change the number of forces, gives error
torque = [0.0]*15
start = 0.0
end = 30.0
t = np.linspace(start,end,num = 100*int((end-start)))
tsize = int((end-start))*100




##the values correspond directly to index1,index2,index3,little1,little2,.... since others are fixed

q_pos_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_vel_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_acc_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_pos = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_vel = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_tor = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

e = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 
ee = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

plot = [[0]]*tsize

kp = 3
kd = 0.03

with open('waypoints.txt') as json_file:
  qp = json.load(json_file)
with open('velocity.txt') as json_file:
  qv = json.load(json_file)


for i in range(len(t)):
    for val in range(15):
        q_pos_desired[val][i] = qp[val][i]
        q_vel_desired[val][i] = qv[val][i]

#with open('desPos.txt') as json_file:
#  q_pos_desired = json.load(json_file)
#
#with open('desVel.txt') as json_file:
#  q_vel_desired = json.load(json_file)
#
#with open('desAcc.txt') as json_file:
#  q_acc_desired = json.load(json_file)


# Do Torque Control:
for i in range(len(t)):
    obj_pos_d = []
    for val in range(15):
        obj_pos_d.append(q_pos_desired[val][i])
    obj_vel_d = []
    for val in range(15):
        obj_vel_d.append(q_vel_desired[val][i])

   

    
    # Read Sensor States:
    joint_states = bullet.getJointStates(id_robot, id_revolute_joints)       
    for j in range(15):
        q_pos[j][i] = joint_states[j][0]
        q_vel[j][i] = joint_states[j][1]    

    # Computing the torque from inverse dynamics:
    obj_pos = []
    for val in range(15):
        obj_pos.append(q_pos[val][i])

    obj_vel = []
    for val in range(15):
        obj_vel.append(q_vel[val][i])
    # for j in range(15):
    #     e[j][i] = q_pos_desired[j][i]-q_pos[j][i]
    #     ee[j][i] = q_vel_desired[j][i]-q_vel[j][i]

     # e = np.add(obj_pos , np.multiply(-1,[q_pos_desired[0][i], q_pos_desired[1][i]]))
    # ee = np.add(obj_vel , np.multiply(-1,[q_vel_desired[0][i], q_vel_desired[1][i]]))
    obj_acc = []
    for val in range(15):
        if i != 0:
            q_acc_desired[val][i] = (q_vel_desired[val][i] - q_vel_desired[val][i-1])/delta_t # - kp*e[val][i] -kd*ee[val][i]
        else:
            q_acc_desired[val][i] = 0
        obj_acc.append(q_acc_desired[val][i])

    for j in range(15):
        e[j][i] = q_pos_desired[j][i]-q_pos[j][i]
        ee[j][i] = q_vel_desired[j][i]-q_vel[j][i]

    kp = 1.8
    kd = 0.05
    kp_1 = 1.6
    kd_1 = 0.02
    Kp_Array = [kp_1,kp,kp,kp_1,kp,kp,kp_1,kp,kp,kp_1,kp,kp,kp_1,kp,kp]
    Kd_Array = [kd_1,kd,kd,kd_1,kd,kd,kd_1,kd,kd,kd_1,kd,kd,kd_1,kd,kd]

    torque = bullet.calculateInverseDynamics(id_robot, obj_pos, obj_vel, obj_acc)
    torque = torque + np.dot(np.array(bullet.calculateMassMatrix(id_robot, obj_pos)),((np.multiply(Kp_Array,np.transpose(np.array(e))[i]) + np.multiply(Kd_Array,np.transpose(np.array(ee))[i]))))
    

    for val in range(15):
        q_tor[val][i] = torque[val]

    #Torque Control for each finger
    bullet.setJointMotorControlArray(id_robot,
                                        id_revolute_joints,
                                        bullet.TORQUE_CONTROL,
                                        forces = [torque[0], torque[1],torque[2],torque[3],
                                        torque[4], torque[5],torque[6],torque[7],
                                        torque[8], torque[9],torque[10],torque[11],
                                        torque[12],torque[13],torque[14]])


    bullet.stepSimulation()

colours = ['red',
            'brown',
            'cyan',
            'red',
            'chocolate',
            'darkorange',
            'red',
            'goldenrod',
            'gold',
            'red',
            'yellowgreen',
            'plum',
            'red',
            'darkgreen',
            'lime']

joint_labels = ['Index_1',
        	    'Index_2',
       	        'Index_3',
                'Little_1',
                'Little_2',
                'Little_3',
                'Middle_1',
                'Middle_2',
                'Middle_3',
                'Ring_1',
                'Ring_2',
                'Ring_3',
                'Thumb_1',
                'Thumb_2',
                'Thumb_3']
if plot:
    # figure1 = plt.figure(figsize=[15, 15])
    # figure2 = plt.figure(figsize=[15, 15])
    # figure3 = plt.figure(figsize=[15, 15])
    # figure4 = plt.figure(figsize=[15, 15])
    # figure5 = plt.figure(figsize=[15, 15])
    # ax_pos = figure1.add_subplot(111)
    # ax_pos.set_title("Joint Position")
    # for i in range(15):
    #   ax_pos.plot(t, q_pos_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # for i in range(15):
    #   ax_pos.plot(t, q_pos[i], color = colours[i],linestyle = 'solid', lw=1, label='Measured q'+str(i))
    #   ax_pos.set_ylim(-1, 1)
    #   ax_pos.legend()
    # ax_vel = figure2.add_subplot(111)
    # ax_vel.set_title("Joint Velocity")
    # for i in range(15):
    #   ax_vel.plot(t, q_vel_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # for i in range(15):
    #   ax_vel.plot(t, q_vel[i], color = colours[i],linestyle = 'solid', lw=1, label='Mesured q'+str(i))
    # ax_vel.set_ylim(-5., 5.)
    # ax_vel.legend()
    # ax_acc = figure3.add_subplot(111)
    # ax_acc.set_title("Joint Acceleration")
    # for i in range(15):
    #  ax_acc.plot(t, q_acc_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # ax_acc.set_ylim(-10., 10.)
    # ax_acc.legend()

    # for ii in range(15):
    fig = plt.figure(figsize=[15, 15])
    ax_tor = fig.add_subplot(111)
    ax_tor.set_title("Error")
    ax_tor.plot(t, e[0], color = colours[0],linestyle = 'solid', lw=2, label=joint_labels[0])
    ax_tor.set_ylim(-0.35, 0.35)
    ax_tor.legend()


    # ax_tor = figure1.add_subplot(111)
    # ax_tor.set_title("Error")
    # ax_tor.plot(t, e[1], color = colours[0],linestyle = 'solid', lw=2, label='Torque q'+str(i))
    # ax_tor.set_ylim(-0.005, 0.005)
    # ax_tor.legend()
    # ax_tor = figure5.add_subplot(111)
    # ax_tor.set_title("Executed Torque")
    # for i in range(15):
    #   ax_tor.plot(t, q_tor[i], color = colours[i],linestyle = 'solid', lw=2, label='Torque q'+str(i))
    # ax_tor.set_ylim(-0.005, 0.005)
    # ax_tor.legend()
    plt.pause(0.01)

while (1):
    time.sleep(0.01)