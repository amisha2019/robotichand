# Robotic Hand RRC Project
<br>
 
### Prerequisites

- Python3 and pybullet

		sudo apt-get install python3.6
		sudo apt-get install python3-pip
		pip3 install pybullet


	Documentation for pybullet: 
	<http://dirkmittler.homeip.net/blend4web_ce/uranium/bullet/docs/pybullet_quickstartguide.pdf>
<br>

### Installation Instructions:

	git clone https://gitlab.com/amisha2019/robotichand.git
<br>
### Details of Project

** URDF Models Involved: **

- Hand.urdf
- Cube.urdf
- Cylinder.urdf
- Sphere2red.urdf
- Plane.urdf

** Graphing Tools Used: **

- Matplotlib & numpy in python

** Simulation Software Used: **

- PyBullet

** Objective: **

- Torque control for grasping motion

** Important Variables: **

- Desired Position: q_pos_desired
- Desired Velocity: q_vel_desired
- Desired Acceleration: q_acc_desired
- Actual Position: q_pos
- Actual Velocity: q_pos
- Actual Acceleration: q_pos

** Joints used in Control System:**

- Arm_Base0
- Arm_1
- Arm_2
- Palm
- Index_0
- Index_1
- Index_2
- Index_3
- Little_0
- Little_1
- Little_2
- Little_3
- Middle_0
- Middle_1
- Middle_2
- Middle_3
- Ring_0
- Ring_1
- Ring_2
- Ring_3
- Thumb_0
- Thumb_1
- Thumb_2
- Thumb_3

Total Number of joints involved: 24
<br>

### Working Principle

We run the simulation for certain timesteps. 
In each timestep we calculate the desired positions, velocities and accelerations for each and every joint in the hand. The desired positions for the arm joints are as:
		
> q_pos_desired[0][s] = 0 
> q_pos_desired[1][s] = 0 
> q_pos_desired[2][s] = 0
> q_pos_desired[3][s] = 0

The desired positions for the finger joints are defined to be the same for each finger. The defined values are as:
> q_pos_desired[ii][s] =0
> q_pos_desired[ii+1][s] = math.sin((1.)/(6.) * math.pi * t[s]) - t[s] 
> q_pos_desired[ii+2][s] =  math.sin((1.)/(4.) * math.pi * t[s])
> q_pos_desired[ii+3][s] =  math.sin((1.)/(4.) * math.pi * t[s]) - 1.0

The desired velocities and accelerations are kept as the derivatives and the double derivatives of the above defined values.

Then using this function we get the actual positions, velocities and accelerations of each of the joints:
> joint_states = bullet.getJointStates(id_robot, id_revolute_joints)

Then we find the torque array using Inverse Dynamics, the function used is as:
> torque = bullet.calculateInverseDynamics(id_robot, obj_pos, obj_vel, obj_acc)

To give the joint torque to all joints, the following function is used:

> bullet.setJointMotoryControlArray(id_robot, id_revolute_joints, bullet.TORQUE_CONTROL, forces = [  ] )

forces[] is a 24 length 1-D array that contains all the torque values to be given for the joints corresponding to their indexes.
<br>

### Graphs Plotted

- Actual Position vs Desired Position
- Actual Velocity vs Desired Velocity
- Actual joint acceleration
- Executed Torque

This can be produced by running the file id.py as:

		python3 id.py

<br>

### Working of Hand

For observing the grasping action of the following objects, run:
 
- Cylinder:
 
		python3 testcylinder.py
		
- Cube: 

		python3 testcube.py
		
- Sphere: 

		python3 testball.py







