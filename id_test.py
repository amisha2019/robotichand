import pybullet as bullet
import time
import numpy as np
import matplotlib.pyplot as plt
import math
import json
plot = True

verbose = False

def present(id):
    chk = [5,6,7,9,10,11,13,14,15,17,18,19,21,22,23]
    for var in range(15):
        if id == chk[var]:
            return True
    return False

# Parameters:
robot_base = [0., 0., 0.]
robot_orientation = [0., 0., 0., 1.]
delta_t = 0.01

# Initialize Bullet Simulator
id_simulator = bullet.connect(bullet.GUI)  # or bullet.DIRECT for non-graphical version
bullet.setTimeStep(delta_t)

#do not change id revolute joints, gave error
id_revolute_joints = [5,6,7,9,10,11,13,14,15,17,18,19,21,22,23]
id_robot = bullet.loadURDF("urdf/fingers.urdf",
                             robot_base,
                             robot_orientation,
                             globalScaling=0.50,
                             useFixedBase=True)

# Disable the motors for torque control:
bullet.setJointMotorControlArray(id_robot,
                                 id_revolute_joints,
                                 bullet.POSITION_CONTROL,
                                 forces = [0.0, 0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0,0.0, 0.0, 0.0, 0.0,0.0]) #do not change the number of forces, gives error

start = 0.0
end = 2.5
t = np.linspace(start,end,num = 100*int((end-start)))
tsize = 100*int((end-start))

##the values correspond directly to index1,index2,index3,little1,little2,.... since others are fixed

q_pos_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_vel_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_acc_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_pos = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_vel = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_tor = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 
#2-D Array (Each time step(row) will contain 23 joint terms to be plotted)
plot = [[0]]*tsize


# for s in range(tsize):
#     t[s] = start + s * delta_t  #t[] contains each time step value - 0,0.001,0.002,...

#     ii = 0
#     while(ii<=14):
#         q_pos_desired[ii][s] = math.sin((1.)/(6.) * math.pi * t[s]) - t[s]
#         q_pos_desired[ii+1][s] =  math.sin((1.)/(4.) * math.pi * t[s]) 
#         q_pos_desired[ii+2][s] =  math.sin((1.)/(4.) * math.pi * t[s]) - 1.0
#         ii = ii+3

#     ii = 0
#     while(ii<=14):
#         q_vel_desired[ii][s] = (math.pi) *(1.)/(6.)*math.cos((1.)/(6.) * math.pi * t[s]) - 1
#         q_vel_desired[ii+1][s] = (math.pi)*(1.)/(4.)*math.cos((1.)/(4.) * math.pi * t[s]) 
#         q_vel_desired[ii+2][s] = (math.pi)*(1.)/(4.)*math.cos((1.)/(4.) * math.pi * t[s])
#         ii = ii+3

#     ii = 0
#     while(ii<=14):
#         q_acc_desired[ii][s] = (-1)*(math.pi)*(math.pi)*(1.)/(36.)*math.sin((1.)/(6.) * math.pi * t[s])
#         q_acc_desired[ii+1][s] = (-1)*(math.pi)*(math.pi)*(1.)/(16.)*math.sin((1.)/(4.) * math.pi * t[s])
#         q_acc_desired[ii+2][s] = (-1)*(math.pi)*(math.pi)*(1.)/(16.)*math.sin((1.)/(4.) * math.pi * t[s])
#         ii = ii+3

with open('desPos.txt') as json_file:
  q_pos_desired = json.load(json_file)

with open('desVel.txt') as json_file:
  q_vel_desired = json.load(json_file)

with open('desAcc.txt') as json_file:
  q_acc_desired = json.load(json_file)

v = 0
# Do Torque Control:
for i in range(len(t)):
    # Read Sensor States:
    joint_states = bullet.getJointStates(id_robot, id_revolute_joints)       
    for j in range(15):
        q_pos[j][i] = joint_states[j][0]
        q_vel[j][i] = joint_states[j][1]    

    # Computing the torque from inverse dynamics:
    obj_pos = []
    for val in range(15):
        obj_pos.append(q_pos[val][i])

    obj_vel = []
    for val in range(15):
        obj_vel.append(q_vel[val][i])

    obj_acc = []
    for val in range(15):
        obj_acc.append(q_acc_desired[val][i])
    if (verbose):
        print("calculateInverseDynamics")
        print("id_robot")
        print(id_robot)
        print("obj_pos")
        print(obj_pos)
        print("obj_vel")
        print(obj_vel)
        print("obj_acc")
        print(obj_acc)

    kp = 3
    kd = 0.03
    #1-D Arrays
    e = [0]*15
    ee = [0]*15
    for j in range(15):
        e[j] = q_pos_desired[j][i]-q_pos[j][i]
        ee[j] = q_vel_desired[j][i]-q_vel[j][i]
    massMatrix= bullet.calculateMassMatrix(id_robot,obj_pos)
    if v == 0:
        print(massMatrix)
        v = 1
    #2-D Array with 24 rows
    result1 = [[0]]*15  #For e 
    result2 = [[0]]*15  #For ee
    for j in range(15):
        for k in range(15):
            result1[j][0] += massMatrix[j][k] * e[k]

    for j in range(15):
        for k in range(15):
            result2[j][0] += massMatrix[j][k] * ee[k]

    H = (kp*np.array(result1) - kd*np.array(result2))
    H = np.transpose(H)
    torque = (np.array(bullet.calculateInverseDynamics(id_robot, obj_pos, obj_vel, obj_acc)) - H).tolist()
    torque = torque[0]

    plot[i] = ee[1]
    # 5,6,7,9,10,11,13,14,15,17,18,19,21,22,23
    q_tor[0][i] = torque[0]
    q_tor[1][i] = torque[1]
    q_tor[2][i] = torque[2]
    q_tor[3][i] = torque[3]
    q_tor[4][i] = torque[4]
    q_tor[5][i] = torque[5]
    q_tor[6][i] = torque[6]
    q_tor[7][i] = torque[7]
    q_tor[8][i] = torque[8]
    q_tor[9][i] = torque[9]
    q_tor[10][i] = torque[10]
    q_tor[11][i] = torque[11]
    q_tor[12][i] = torque[12]
    q_tor[13][i] = torque[13]
    q_tor[14][i] = torque[14]

    if (verbose):
        print("torque=")
        print(torque)

    # Set the Joint Torques:
    bullet.setJointMotorControlArray(id_robot,
                                        id_revolute_joints,
                                        bullet.TORQUE_CONTROL,
                                        forces=[torque[0], torque[1], torque[2],
                                                torque[3],torque[4],torque[5],torque[6],torque[7],
                                                torque[8],torque[9],torque[10],torque[11],
                                                torque[12],torque[13],torque[14]])

    # Step Simulation
    bullet.stepSimulation()

if plot:
    figure1 = plt.figure(figsize=[15, 15])
    #figure2 = plt.figure(figsize=[15, 15])
    #figure3 = plt.figure(figsize=[15, 15])
    #figure4 = plt.figure(figsize=[15, 15])

    # ax_pos = figure1.add_subplot(111)
    # ax_pos.set_title("Joint Position")
    # for i in joints:
    #   ax_pos.plot(t, q_pos_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # for i in joints:
    #   ax_pos.plot(t, q_pos[i], color = colours[i],linestyle = 'solid', lw=1, label='Measured q'+str(i))
    #   ax_pos.set_ylim(-1., 1.)
    #   ax_pos.legend()

    ax_pos = figure1.add_subplot(111)
    ax_pos.set_title("error")
    ax_pos.plot(t, plot,linestyle = 'dashed', lw=2)

    # ax_vel = figure2.add_subplot(111)

    # ax_vel.set_title("Joint Velocity")
    # for i in joints:
    #   ax_vel.plot(t, q_vel_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # for i in joints:
    #   ax_vel.plot(t, q_vel[i], color = colours[i],linestyle = 'solid', lw=1, label='Mesured q'+str(i))
    # ax_vel.set_ylim(-2., 2.)
    # ax_vel.legend()

    # ax_acc = figure3.add_subplot(111)
    # ax_acc.set_title("Joint Acceleration")
    # for i in joints:
    #   ax_acc.plot(t, q_acc_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # ax_acc.set_ylim(-10., 10.)
    # ax_acc.legend()

    # ax_tor = figure4.add_subplot(111)
    # ax_tor.set_title("Executed Torque")
    # for i in joints:
    #   ax_tor.plot(t, q_tor[i], color = colours[i],linestyle = 'solid', lw=2, label='Torque q'+str(i))
    # ax_tor.set_ylim(-20., 20.)
    # ax_tor.legend()

    plt.pause(0.01)

while (1):
    bullet.stepSimulation()
    time.sleep(0.01)