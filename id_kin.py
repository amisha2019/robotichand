import pybullet as bullet
import time
import json
# Initialize Bullet Simulator
# Damped Least Squares Inverse Kinematics (damping is too high)
id_simulator = bullet.connect(bullet.GUI)  # or bullet.DIRECT for non-graphical version
delta_t = 0.01
bullet.setTimeStep(delta_t)
bullet.setGravity(0,0,-49.8)
cubeStartPos = [1,0,0]
cyaw=-30
cpitch=-35
cdist=4.0
bullet.resetDebugVisualizerCamera( cameraDistance=cdist, cameraYaw=cyaw, cameraPitch=cpitch, cameraTargetPosition=(0.0, -0.35, 0.20))
cubeStartOrientation = bullet.getQuaternionFromEuler([0,0,1.5])
robot_base = [0., 0., 0.]
robot_orientation = [0., 0., 0., 1.]
id_revolute_joints = [5,6,7,9,10,11,13,14,15,17,18,19,21,22,23]
plane = bullet.loadURDF("urdf/plane.urdf")
id_robot = bullet.loadURDF("urdf/fingers.urdf",
                             robot_base,
                             robot_orientation,
                             globalScaling=0.50,
                             useFixedBase=True)
IDPick = bullet.loadURDF("urdf/Cube.urdf",[-0.05,-1.6,1.0],useFixedBase=True)   
cubePos, cubeOrn = bullet.getBasePositionAndOrientation(IDPick)
print(cubePos)
start = 0.0
end = 5.0

steps = int((end - start) / delta_t)
# 7,11,15,19,23
endeff_a = cubePos
endeff_b = [-0.15,-1.5,0.53]
endeff_c = [-0.15,-1.5,0.53]
endeff_d = [-0.15,-1.5,0.53]
endeff_e = [-0.15,-1.5,0.53]
val1 = bullet.calculateInverseKinematics(id_robot, 7, endeff_a)
val2 = bullet.calculateInverseKinematics(id_robot, 11, endeff_a)
val3 = bullet.calculateInverseKinematics(id_robot, 15, endeff_a)
val4 = bullet.calculateInverseKinematics(id_robot, 19, endeff_a)
val5 = bullet.calculateInverseKinematics(id_robot, 23, endeff_a)
print(val1)
print(val2)
print(val3)
print(val4)
print(val5)
bullet.setJointMotorControlArray(id_robot,
                                 id_revolute_joints,
                                 bullet.POSITION_CONTROL,
                                 targetPositions = [val1[0],val1[1], 0.0,val2[3],val2[4], 0.0,val3[6],val3[7], 0.0,val4[9],val4[10], 0.0,val5[12],val5[13], 0.0])
while(1):
    bullet.stepSimulation()
    time.sleep(0.01)
joint_states = bullet.getJointStates(id_robot, id_revolute_joints)
pos = [0]*15
vel = [0]*15
for i in range(15):
    pos[i] = joint_states[i][0]
    vel[i] = joint_states[i][1]

with open('ikpos.txt', 'w') as outfile:
    json.dump(pos,outfile)

with open('ikvel.txt', 'w') as outfile:
    json.dump(vel,outfile)