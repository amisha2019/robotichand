import pybullet as bullet
plot = True
import time

if (plot):
  import matplotlib.pyplot as plt
import math
verbose = False

# Parameters:
robot_base = [0., 0., 0.]
robot_orientation = [0., 0., 0., 1.]
delta_t = 0.001

# Initialize Bullet Simulator
id_simulator = bullet.connect(bullet.GUI)  # or bullet.DIRECT for non-graphical version
bullet.setTimeStep(delta_t)

cyaw=-30
cpitch=-35
cdist=4.0
bullet.resetDebugVisualizerCamera( cameraDistance=cdist, cameraYaw=cyaw, cameraPitch=cpitch, cameraTargetPosition=(0.0, -0.35, 0.20))

cylinderIDPick = bullet.loadURDF("urdf/sphere2red.urdf",[-0.12,-1.73,1.25],bullet.getQuaternionFromEuler([1.57075,1.57075,1.57075]))


id_revolute_joints = [0,1,2,3,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
plane = bullet.loadURDF("urdf/plane.urdf")
#boxIDPick = bullet.loadURDF("urdf/Cylinder.urdf",[0,-1.55,0.85],bullet.getQuaternionFromEuler([1.57075,1.57075,1.57075]))
id_robot = bullet.loadURDF("urdf/hand.urdf",
                             robot_base,
                             robot_orientation,
                             globalScaling=0.50,
                             useFixedBase=True)


#bullet.changeDynamics(id_robot, -1, linearDamping=0, angularDamping=0)
#bullet.changeDynamics(id_robot, 0, linearDamping=0, angularDamping=0)
#bullet.changeDynamics(id_robot, 1, linearDamping=0, angularDamping=0)

jointTypeNames = [
    "JOINT_REVOLUTE", "JOINT_PRISMATIC", "JOINT_SPHERICAL", "JOINT_PLANAR", "JOINT_FIXED",
    "JOINT_POINT2POINT", "JOINT_GEAR"
]

# Disable the motors for torque control:
bullet.setJointMotorControlArray(id_robot,
                                 id_revolute_joints,
                                 bullet.VELOCITY_CONTROL,
                                 forces=[0.0, 0.0, 0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0, 0.0])

# Target Positions:
start = 0.0
end = 3.0

steps = int((end - start) / delta_t)
t = [0] * steps
q_pos_desired = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 

q_vel_desired = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 

q_acc_desired = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 


for s in range(steps):
   t[s] = start + s * delta_t
   q_pos_desired[0][s] = 0
   q_pos_desired[1][s] = 0 #math.sin((1.)/(2.) * math.pi * t[s]) - t[s]
   q_pos_desired[2][s] = 0 #math.sin( (1.)/(4.) * math.pi * t[s]) 
   q_pos_desired[3][s] = 0 #math.sin( (1.)/(4.) * math.pi * t[s])# (math.cos(math.pi * t[s]) - 1.0)

   q_vel_desired[0][s] = 0
   q_vel_desired[1][s] = 0#(math.pi) *(1.)/(2.)*math.cos((1.)/(2.) * math.pi * t[s]) - 1
   q_vel_desired[2][s] = 0#(math.pi) *(1.)/(4.)*math.cos((1.)/(2.) * math.pi * t[s]) 
   q_vel_desired[3][s] = 0#(math.pi) *(1.)/(4.)*math.cos((1.)/(2.) * math.pi * t[s])#(-1)*(math.pi)* math.sin(math.pi * t[s]) 

   q_acc_desired[0][s] = 0
   q_acc_desired[1][s] = 0#(-1)*(math.pi)*(math.pi)*(1.)/(4.)*math.sin((1.)/(2.) * math.pi * t[s])
   q_acc_desired[2][s] = 0#(-1)*(math.pi)*(math.pi)*(1.)/(8.)*math.sin((1.)/(2.) * math.pi * t[s])
   q_acc_desired[3][s] = 0#(-1)*(math.pi)*(math.pi)*(1.)/(8.)*math.sin((1.)/(2.) * math.pi * t[s])#(math.pi)*(math.pi)*math.cos(math.pi*t[s])

   ii = 4
   while(ii<=23):
       a = 1
       if(ii == 20):
           a = -1
       q_pos_desired[ii][s] =0# math.sin((1.)/(6.) * math.pi * t[s]) - t[s] #math.sin((1.)/(4.) * math.pi * t[s]) 
       q_pos_desired[ii+1][s] = a*math.sin((1.)/(6.) * math.pi * t[s]) - t[s]
       q_pos_desired[ii+2][s] =  a*math.sin((1.)/(4.) * math.pi * t[s]) 
       q_pos_desired[ii+3][s] =  a*math.sin((1.)/(4.) * math.pi * t[s]) - 1.0
       ii = ii+4

   ii = 4
   while(ii<=23):
       a = 1
       if(ii == 20):
           a = -1
       q_vel_desired[ii][s] =0# (math.pi) *(1.)/(6.)*math.cos((1.)/(6.) * math.pi * t[s]) - 1 #(math.pi)*(1.)/(4.)*math.cos((1.)/(4.) * math.pi * t[s])
       q_vel_desired[ii+1][s] = a*(math.pi) *(1.)/(6.)*math.cos((1.)/(6.) * math.pi * t[s]) - 1
       q_vel_desired[ii+2][s] = a*(math.pi)*(1.)/(4.)*math.cos((1.)/(4.) * math.pi * t[s]) 
       q_vel_desired[ii+3][s] = a*(math.pi)*(1.)/(4.)*math.cos((1.)/(4.) * math.pi * t[s])
       ii = ii+4
 
   ii = 4
   while(ii<=23):
       a = 1
       if(ii == 20):
           a = -1
       q_acc_desired[ii][s] =0# (-1)*(math.pi)*(math.pi)*(1.)/(36.)*math.sin((1.)/(6.) * math.pi * t[s])# (-1)*(math.pi)*(math.pi)*(1.)/(16.)*math.sin((1.)/(4.) * math.pi * t[s])
       q_acc_desired[ii+1][s] = a*(-1)*(math.pi)*(math.pi)*(1.)/(36.)*math.sin((1.)/(6.) * math.pi * t[s])
       q_acc_desired[ii+2][s] = a*(-1)*(math.pi)*(math.pi)*(1.)/(16.)*math.sin((1.)/(4.) * math.pi * t[s])
       q_acc_desired[ii+3][s] = a*(-1)*(math.pi)*(math.pi)*(1.)/(16.)*math.sin((1.)/(4.) * math.pi * t[s])
       ii = ii+4


q_pos = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 

q_vel = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 

q_tor = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 


# Do Torque Control:
for i in range(len(t)):
   # Read Sensor States:
   joint_states = bullet.getJointStates(id_robot, id_revolute_joints)
 
   val = 0;
   while(val<=23):
       q_pos[val][i] = joint_states[val][0]
       val = val+1
 
   a = joint_states[1][0]
   q_pos[1][i] = a
   val = 0;
   while(val<=23):
       q_vel[val][i] = joint_states[val][1]
       val = val+1
 
   # Computing the torque from inverse dynamics:
   obj_pos = []
   for val in range(24):
       obj_pos.append(q_pos[val][i])
 
   obj_vel = []
   for val in range(24):
       obj_vel.append(q_vel[val][i])
 
   obj_acc = []
   for val in range(24):
       obj_acc.append(q_acc_desired[val][i])


   if (verbose):
      print("calculateInverseDynamics")
      print("id_robot")
      print(id_robot)
      print("obj_pos")
      print(obj_pos)
      print("obj_vel")
      print(obj_vel)
      print("obj_acc")
      print(obj_acc)

   torque = bullet.calculateInverseDynamics(id_robot, obj_pos, obj_vel, obj_acc)
   q_tor[0][i] = torque[0]
   q_tor[1][i] = torque[1]
   q_tor[2][i] = torque[2]
   q_tor[3][i] = torque[3]
   q_tor[4][i] = torque[4]
   q_tor[5][i] = torque[5]
   q_tor[6][i] = torque[6]
   q_tor[7][i] = torque[7]
   q_tor[8][i] = torque[8]
   q_tor[9][i] = torque[9]
   q_tor[10][i] = torque[10]
   q_tor[11][i] = torque[11]
   q_tor[12][i] = torque[12]
   q_tor[13][i] = torque[13]
   q_tor[14][i] = torque[14]
   q_tor[15][i] = torque[15]
   q_tor[16][i] = torque[16]
   q_tor[17][i] = torque[17]
   q_tor[18][i] = torque[18]
   q_tor[19][i] = torque[19]
   q_tor[20][i] = torque[20]
   q_tor[21][i] = torque[21]
   q_tor[22][i] = torque[22]
   q_tor[23][i] = torque[23]
  

  
   if (verbose):
      print("torque=")
      print(torque)

  # Set the Joint Torques:
   bullet.setJointMotorControlArray(id_robot,
                                     id_revolute_joints,
                                     bullet.TORQUE_CONTROL,
                                     forces=[torque[0], torque[1],torque[2], torque[3], torque[4], torque[5], torque[6],
                                             torque[7],torque[8],torque[9],torque[10],torque[11],torque[12],torque[13],
                                             torque[14],torque[15],torque[16],torque[17],torque[18],torque[19],torque[20],
                                             torque[21],torque[22],torque[23]])

  # Step Simulation
   bullet.stepSimulation()

# Plot the Position, Velocity and Acceleration:
colours = ['black',
            'brown',
            'red',
            'coral',
            'chocolate',
            'darkorange',
            'tan',
            'goldenrod',
            'gold',
            'fuchsia',
            'yellowgreen',
            'plum',
            'palegreen',
            'darkgreen',
            'lime',
            'turquoise',
            'cyan',
            'skyblue',
            'dodgerblue',
            'slategray',
            'cornflowerblue',
            'lavender',
            'midnightblue',
            'mediumpurple',
            'magenta']
            
joints = list(range(24))


while (1):
  bullet.stepSimulation()
  time.sleep(0.01)
