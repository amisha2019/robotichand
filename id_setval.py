import pybullet as bullet
import time
import numpy as np
import matplotlib.pyplot as plt
import math
import json


with open('ikpos.txt') as json_file:
  qp = json.load(json_file)
with open('ikvel.txt') as json_file:
  qv = json.load(json_file)
  
start = 0.0
end = 5.0
tsize = 100*int((end-start))

q_pos_desired = [[0.] * 6*tsize, [0.] * 6*tsize, [0.] * 6*tsize,[0.] * 6*tsize,[0.] * 6*tsize,
                [0.] * 6*tsize,[0.] * 6*tsize,[0.] *6* tsize,[0.] *6* tsize,[0.] *6* tsize,
                [0.] *6* tsize,[0.] *6* tsize,[0.] *6* tsize,[0.] *6* tsize,[0.] *6* tsize] 

q_vel_desired = [[0.] * 6*tsize, [0.] * 6*tsize, [0.] * 6*tsize,[0.] * 6*tsize,[0.] * 6*tsize,
                [0.] * 6*tsize,[0.] * 6*tsize,[0.] *6* tsize,[0.] *6* tsize,[0.] *6* tsize,
                [0.] *6* tsize,[0.] *6* tsize,[0.] *6* tsize,[0.] *6* tsize,[0.] *6* tsize] 

for jj in range(3):
    for i in range(tsize): #0 to tsize-1
        for val in range(15):
            q_pos_desired[val][i+2*jj*tsize] = (qp[val]/tsize)*i
            q_vel_desired[val][i+2*jj*tsize] = (qv[val]/tsize)*i

    for i in range(tsize):
        for val in range(15):
            q_pos_desired[val][i+tsize+2*jj*tsize] = q_pos_desired[val][tsize-i-1+2*jj*tsize] #tsize to 2tsize-1
            q_vel_desired[val][i+tsize+2*jj*tsize] = -q_vel_desired[val][tsize-i-1+2*jj*tsize]
        
        
with open('waypoints.txt', 'w') as outfile:
    json.dump(q_pos_desired,outfile)

with open('velocity.txt', 'w') as outfile:
    json.dump(q_vel_desired,outfile)       
        