import pybullet as p
import pybullet_data
import time
physicsClient = p.connect(p.GUI)
p.setAdditionalSearchPath(pybullet_data.getDataPath()) 
cyaw=170 #rotating along y axis
cpitch=-35
cdist=10
p.resetDebugVisualizerCamera( cameraDistance=cdist, cameraYaw=cyaw, cameraPitch=cpitch, cameraTargetPosition=(0.0, -0.35, 0.20))
p.setGravity(0,0,-10)
cubeStartPos = [1,0,0]
cubeStartOrientation = p.getQuaternionFromEuler([0,0,1.5])
boxId = p.loadURDF("urdf/hand.urdf",cubeStartPos, cubeStartOrientation)
p.loadURDF("urdf/plane.urdf")
boxIDPick = p.loadURDF("urdf/Cylinder.urdf",[3.75,0,0.2],p.getQuaternionFromEuler([1.57075,0,0])) #first one modifies it in required fashion
for j in range(p.getNumJoints(boxId)):
    jointinfo= p.getJointInfo(boxId,j)
    print(jointinfo)
for t in range(100000):
    p.stepSimulation()
    time.sleep(1/1000)
cubePos, cubeOrn = p.getBasePositionAndOrientation(boxId)
print(cubePos,cubeOrn)

p.disconnect()
