import pybullet as bullet
import time
import numpy as np
import matplotlib.pyplot as plt
import math
import json
plot = True


font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

plt.rc('font', **font)

verbose = False

# Parameters:
robot_base = [0., 0., 0.]
robot_orientation = [0., 0., 0., 1.]
delta_t = 0.01

# Initialize Bullet Simulator
id_simulator = bullet.connect(bullet.GUI)  # or bullet.DIRECT for non-graphical version
bullet.setTimeStep(delta_t)

#do not change id revolute joints, gave error
id_revolute_joints = [5,6,7,9,10,11,13,14,15,17,18,19,21,22,23]
id_robot = bullet.loadURDF("urdf/fingers.urdf",
                             robot_base,
                             robot_orientation,
                             globalScaling=0.50,
                             useFixedBase=True)

# Disable the motors for torque control:
bullet.setJointMotorControlArray(id_robot,
                                 id_revolute_joints,
                                 bullet.POSITION_CONTROL,
                                 forces = [0.0, 0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0,0.0, 0.0, 0.0, 0.0,0.0]) #do not change the number of forces, gives error

start = 0.0
end = 30.0
t = np.linspace(start,end,num = 100*int((end-start)))
tsize = 100*int((end-start))

##the values correspond directly to index1,index2,index3,little1,little2,.... since others are fixed

q_pos_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_vel_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_acc_desired = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
                [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_pos = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_vel = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

q_tor = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

e = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 
ee = [[0.] * tsize, [0.] * tsize, [0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,
        [0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize,[0.] * tsize] 

plot = [[0]]*tsize

kp = 3
kd = 0.03


with open('waypoints.txt') as json_file:
  qp = json.load(json_file)
with open('velocity.txt') as json_file:
  qv = json.load(json_file)

print(len(qp))
print(len(qp[0]))
print(len(qv))
print(len(qv[0]))

for i in range(len(t)):
    for val in range(15):
        q_pos_desired[val][i] = qp[val][i]
        q_vel_desired[val][i] = qv[val][i]

# Do Torque Control:
for i in range(len(t)):
    obj_pos_d = []
    for val in range(15):
        obj_pos_d.append(q_pos_desired[val][i])
    obj_vel_d = []
    for val in range(15):
        obj_vel_d.append(q_vel_desired[val][i])
    bullet.setJointMotorControlArray(id_robot,
                                        id_revolute_joints,
                                        bullet.POSITION_CONTROL,
                                        forces = [0.06, 0.05, 0.05,0.06,
                                         0.05,0.05, 0.06,0.05, 0.05,0.06,0.05, 0.05, 0.06, 0.05, 0.05],
                                        positionGains = np.array([1.9]*len(id_revolute_joints)),
                                        velocityGains = np.array([0.013]*len(id_revolute_joints)),
                                        targetPositions = obj_pos_d)
    
    # Read Sensor States:
    joint_states = bullet.getJointStates(id_robot, id_revolute_joints)       
    for j in range(15):
        q_pos[j][i] = joint_states[j][0]
        q_vel[j][i] = joint_states[j][1]    

    # Computing the torque from inverse dynamics:
    obj_pos = []
    for val in range(15):
        obj_pos.append(q_pos[val][i])

    obj_vel = []
    for val in range(15):
        obj_vel.append(q_vel[val][i])

    obj_acc = []
    for val in range(15):
        obj_acc.append(q_acc_desired[val][i])
    if (verbose):
        print("calculateInverseDynamics")
        print("id_robot")
        print(id_robot)
        print("obj_pos")
        print(obj_pos)
        print("obj_vel")
        print(obj_vel)
        print("obj_acc")
        print(obj_acc)
    for j in range(15):
        e[j][i] = q_pos_desired[j][i]-q_pos[j][i]
        ee[j][i] = q_vel_desired[j][i]-q_vel[j][i]
    massMatrix= bullet.calculateMassMatrix(id_robot,obj_pos)
    #2-D Array with 24 rows
    result1 = [[0]]*15  #For e 
    result2 = [[0]]*15  #For ee
    for j in range(15):
        for k in range(15):
            result1[j][0] += massMatrix[j][k] * e[k][i]

    for j in range(15):
        for k in range(15):
            result2[j][0] += massMatrix[j][k] * ee[k][i]
    H = (kp*np.array(result1) - kd*np.array(result2))
    H = np.transpose(H)
    torque = (np.array(bullet.calculateInverseDynamics(id_robot, obj_pos, obj_vel, obj_acc)) - H).tolist()
    torque = torque[0]
    for val in range(15):
        q_tor[val][i] = torque[val]
    bullet.stepSimulation()

colours = ['red',
            'brown',
            'cyan',
            'red',
            'chocolate',
            'darkorange',
            'red',
            'goldenrod',
            'gold',
            'red',
            'yellowgreen',
            'plum',
            'red',
            'darkgreen',
            'lime']



joint_labels = ['Index_1',
        	    'Index_2',
       	        'Index_3',
                'Little_1',
                'Little_2',
                'Little_3',
                'Middle_1',
                'Middle_2',
                'Middle_3',
                'Ring_1',
                'Ring_2',
                'Ring_3',
                'Thumb_1',
                'Thumb_2',
                'Thumb_3']



if plot:
    # figure1 = plt.figure(figsize=[15, 15])
    # figure2 = plt.figure(figsize=[15, 15])
    # figure3 = plt.figure(figsize=[15, 15])
    # figure4 = plt.figure(figsize=[15, 15])
    # figure5 = plt.figure(figsize=[15, 15])
    # ax_pos = figure1.add_subplot(111)
    # ax_pos.set_title("Joint Position")
    # for i in range(15):
    #   ax_pos.plot(t, q_pos_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # for i in range(15):
    #   ax_pos.plot(t, q_pos[i], color = colours[i],linestyle = 'solid', lw=1, label='Measured q'+str(i))
    #   ax_pos.set_ylim(-1, 1)
    #   ax_pos.legend()
    # ax_vel = figure2.add_subplot(111)
    # ax_vel.set_title("Joint Velocity")
    # for i in range(15):
    #   ax_vel.plot(t, q_vel_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # for i in range(15):
    #   ax_vel.plot(t, q_vel[i], color = colours[i],linestyle = 'solid', lw=1, label='Mesured q'+str(i))
    # ax_vel.set_ylim(-5., 5.)
    # ax_vel.legend()
    # ax_acc = figure3.add_subplot(111)
    # ax_acc.set_title("Joint Acceleration")
    # for i in range(15):
    #  ax_acc.plot(t, q_acc_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # ax_acc.set_ylim(-10., 10.)
    # ax_acc.legend()

    for ii in range(15):
        fig = plt.figure(figsize=[15, 15])
        ax_tor = fig.add_subplot(111)
        ax_tor.set_title("Error")
        ax_tor.plot(t, e[ii], color = colours[0],linestyle = 'solid', lw=2, label=joint_labels[ii])
        ax_tor.set_ylim(-0.3, 0.3)
        ax_tor.legend()


    # ax_tor = figure1.add_subplot(111)
    # ax_tor.set_title("Error")
    # ax_tor.plot(t, e[1], color = colours[0],linestyle = 'solid', lw=2, label='Torque q'+str(i))
    # ax_tor.set_ylim(-0.005, 0.005)
    # ax_tor.legend()
    # ax_tor = figure5.add_subplot(111)
    # ax_tor.set_title("Executed Torque")
    # for i in range(15):
    #   ax_tor.plot(t, q_tor[i], color = colours[i],linestyle = 'solid', lw=2, label='Torque q'+str(i))
    # ax_tor.set_ylim(-0.005, 0.005)
    # ax_tor.legend()
    plt.pause(0.01)

while (1):
    bullet.stepSimulation()
    time.sleep(0.01)