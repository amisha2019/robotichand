import pybullet as bullet
import time
import numpy as np
import matplotlib.pyplot as plt
import math
import json
plot = True

verbose = True

def present(id):
    chk = [5,6,7,9,10,11,13,14,15,17,18,19,21,22,23]
    for var in range(15):
        if id == chk[var]:
            return True
    return False

# Parameters:
robot_base = [0., 0., 0.]
robot_orientation = [0., 0., 0., 1.]
delta_t = 0.01

# Initialize Bullet Simulator
id_simulator = bullet.connect(bullet.GUI)  # or bullet.DIRECT for non-graphical version
bullet.setTimeStep(delta_t)
cyaw=-30
cpitch=-35
cdist=4.0
bullet.resetDebugVisualizerCamera( cameraDistance=cdist, cameraYaw=cyaw, cameraPitch=cpitch, cameraTargetPosition=(0.0, -0.35, 0.20))
#do not change id revolute joints, gave error
id_revolute_joints = [0,1,2,3,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
id_robot = bullet.loadURDF("urdf/fingers.urdf",
                             robot_base,
                             robot_orientation,
                             globalScaling=0.50,
                             useFixedBase=True,
                             flags = bullet.URDF_USE_SELF_COLLISION | bullet.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)

# Disable the motors for torque control:
bullet.setJointMotorControlArray(id_robot,
                                 id_revolute_joints,
                                 bullet.VELOCITY_CONTROL,
                                 forces=[0.0, 0.0, 0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0,
                                         0.0,0.0, 0.0,0.0, 0.0,0.0, 0.0]) #do not change the number of forces, gives error
q0 = [0]*15
v0 = [0]*15
with open('ikpos.txt') as json_file:
  q1 = json.load(json_file)

with open('ikvel.txt') as json_file:
   v1 = json.load(json_file)
t0 = 0
tf = 2.5
t = np.linspace(t0,tf,num = 100*int((tf-t0)))
steps = 100*int((tf-t0))
#v1 = [0]*15
#q0,v0,q1,v1,t0,tf = map(float,input('Initial Data (enter space separated) = [q0,v0,q1,v1,t0,tf] = ').split())

c = np.ones(steps) #1 X tsize

M = np.array([[1, t0, t0*t0, t0*t0*t0], 
              [0,1,2*t0,3*t0*t0],
              [1, tf, tf*tf, tf*tf*tf], 
              [0,1,2*tf,3*tf*tf]]) # 4 X 4

b = np.array([q0, v0, q1, v1])#should be 4 X 15
a = (np.transpose(M)) @ b #should be 4 X 15

q_pos_desired = np.reshape(a[0],[15,1])*c + np.reshape(a[1],[15,1])*t + np.reshape(a[2],[15,1])*t*t + np.reshape(a[3],[15,1])*t*t*t #should be 15 X tsize
q_vel_desired = np.reshape(a[1],[15,1])*c + 2*np.reshape(a[2],[15,1])*t + 3*np.reshape(a[3],[15,1])*t*t #should be 15 X tsize
q_acc_desired = 2*np.reshape(a[2],[15,1])*c + 6*np.reshape(a[3],[15,1])*t #should be 15 X tsizet = [0] * steps

##the values correspond directly to index1,index2,index3,little1,little2,.... since others are fixed

q_pos = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,
        [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,
        [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 

q_vel = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,
        [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,
        [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 

q_tor = [[0.] * steps, [0.] * steps, [0.] * steps,[0.] * steps,[0.] * steps,
        [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,
        [0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps,[0.] * steps] 
#2-D Array (Each time step(row) will contain 23 joint terms to be plotted)
plot = [[0]]*steps

# Do Torque Control:
for i in range(len(t)):
    # Read Sensor States:
    joint_states = bullet.getJointStates(id_robot, id_revolute_joints)       
    q_pos[0][i] = joint_states[5][0]
    q_pos[1][i] = joint_states[6][0]
    q_pos[2][i] = joint_states[7][0]
    q_pos[3][i] = joint_states[9][0]
    q_pos[4][i] = joint_states[10][0]
    q_pos[5][i] = joint_states[11][0]
    q_pos[6][i] = joint_states[13][0]
    q_pos[7][i] = joint_states[14][0]
    q_pos[8][i] = joint_states[15][0]
    q_pos[9][i] = joint_states[17][0]
    q_pos[10][i] = joint_states[18][0]
    q_pos[11][i] = joint_states[19][0]
    q_pos[12][i] = joint_states[21][0]
    q_pos[13][i] = joint_states[22][0]
    q_pos[14][i] = joint_states[23][0]
    
    q_vel[0][i] = joint_states[5][1]
    q_vel[1][i] = joint_states[6][1]
    q_vel[2][i] = joint_states[7][1]
    q_vel[3][i] = joint_states[9][1]
    q_vel[4][i] = joint_states[10][1]
    q_vel[5][i] = joint_states[11][1]
    q_vel[6][i] = joint_states[13][1]
    q_vel[7][i] = joint_states[14][1]
    q_vel[8][i] = joint_states[15][1]
    q_vel[9][i] = joint_states[17][1]
    q_vel[10][i] = joint_states[18][1]
    q_vel[11][i] = joint_states[19][1]
    q_vel[12][i] = joint_states[21][1]
    q_vel[13][i] = joint_states[22][1]
    q_vel[14][i] = joint_states[23][1]
    

    # Computing the torque from inverse dynamics:
    obj_pos = []
    for val in range(15):
        obj_pos.append(q_pos[val][i])

    obj_vel = []
    for val in range(15):
        obj_vel.append(q_vel[val][i])

    obj_acc = []
    for val in range(15):
        obj_acc.append(q_acc_desired[val][i])
    if (verbose):
        print("calculateInverseDynamics")
        print("id_robot")
        print(id_robot)
        print("obj_pos")
        print(obj_pos)
        print("obj_vel")
        print(obj_vel)
        print("obj_acc")
        print(obj_acc)

    kp = 300
    kd = 30
    #1-D Arrays
    e = [0]*15
    ee = [0]*15
    for j in range(15):
        e[j] = q_pos_desired[j][i]-q_pos[j][i]
        ee[j] = q_vel_desired[j][i]-q_vel[j][i]
    massMatrix= bullet.calculateMassMatrix(id_robot,obj_pos)
    #2-D Array with 24 rows
    result1 = [[0]]*15  #For e 
    result2 = [[0]]*15  #For ee
    for j in range(15):
        for k in range(15):
            result1[j][0] += massMatrix[j][k] * e[k]

    for j in range(15):
        for k in range(15):
            result2[j][0] += massMatrix[j][k] * ee[k]

    H = (kp*np.array(result1) - kd*np.array(result2))
    H = np.transpose(H)
    torque = (np.array(bullet.calculateInverseDynamics(id_robot, obj_pos, obj_vel, obj_acc)) - H).tolist()
    torque = torque[0]

    plot[i] = e[1]
    # 5,6,7,9,10,11,13,14,15,17,18,19,21,22,23
    q_tor[0][i] = torque[0]
    q_tor[1][i] = torque[1]
    q_tor[2][i] = torque[2]
    q_tor[3][i] = torque[3]
    q_tor[4][i] = torque[4]
    q_tor[5][i] = torque[5]
    q_tor[6][i] = torque[6]
    q_tor[7][i] = torque[7]
    q_tor[8][i] = torque[8]
    q_tor[9][i] = torque[9]
    q_tor[10][i] = torque[10]
    q_tor[11][i] = torque[11]
    q_tor[12][i] = torque[12]
    q_tor[13][i] = torque[13]
    q_tor[14][i] = torque[14]

    if (verbose):
        print("torque=")
        print(torque)

    # Set the Joint Torques:
    bullet.setJointMotorControlArray(id_robot,
                                        id_revolute_joints,
                                        bullet.TORQUE_CONTROL,
                                        forces=[0, 0, 0, 0, 0,torque[0], torque[1], torque[2],
                                                0,torque[3],torque[4],torque[5],0,torque[6],torque[7],
                                                torque[8],0,torque[9],torque[10],torque[11],0,
                                                torque[12],torque[13],torque[14]])

    # Step Simulation
    bullet.stepSimulation()

if plot:
    figure1 = plt.figure(figsize=[15, 15])
    #figure2 = plt.figure(figsize=[15, 15])
    #figure3 = plt.figure(figsize=[15, 15])
    #figure4 = plt.figure(figsize=[15, 15])

    # ax_pos = figure1.add_subplot(111)
    # ax_pos.set_title("Joint Position")
    # for i in joints:
    #   ax_pos.plot(t, q_pos_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # for i in joints:
    #   ax_pos.plot(t, q_pos[i], color = colours[i],linestyle = 'solid', lw=1, label='Measured q'+str(i))
    #   ax_pos.set_ylim(-1., 1.)
    #   ax_pos.legend()

    ax_pos = figure1.add_subplot(111)
    ax_pos.set_title("error")
    ax_pos.plot(t, plot,linestyle = 'dashed', lw=2)

    # ax_vel = figure2.add_subplot(111)

    # ax_vel.set_title("Joint Velocity")
    # for i in joints:
    #   ax_vel.plot(t, q_vel_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # for i in joints:
    #   ax_vel.plot(t, q_vel[i], color = colours[i],linestyle = 'solid', lw=1, label='Mesured q'+str(i))
    # ax_vel.set_ylim(-2., 2.)
    # ax_vel.legend()

    # ax_acc = figure3.add_subplot(111)
    # ax_acc.set_title("Joint Acceleration")
    # for i in joints:
    #   ax_acc.plot(t, q_acc_desired[i], color = colours[i],linestyle = 'dashed', lw=2, label='Desired q'+str(i))
    # ax_acc.set_ylim(-10., 10.)
    # ax_acc.legend()

    # ax_tor = figure4.add_subplot(111)
    # ax_tor.set_title("Executed Torque")
    # for i in joints:
    #   ax_tor.plot(t, q_tor[i], color = colours[i],linestyle = 'solid', lw=2, label='Torque q'+str(i))
    # ax_tor.set_ylim(-20., 20.)
    # ax_tor.legend()

    plt.pause(0.01)

while (1):
    bullet.stepSimulation()
    time.sleep(0.01)
