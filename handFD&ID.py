import pybullet_utils.bullet_client as bc
import pybullet
import pybullet_data

import os, time, copy
import numpy as np

np.set_printoptions(precision=5, suppress=True)

hz = 240.0
dt = 1.0 / hz

sim = bc.BulletClient(connection_mode=pybullet.GUI)

sim.setGravity(0, 0, -9.81)
sim.setTimeStep(dt)
sim.setRealTimeSimulation(0)

controlled_joints = [0,1,2,3,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
addtnl_torque = np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
n_dofs = len(controlled_joints)

pybullet.setAdditionalSearchPath(pybullet_data.getDataPath())
urdf_path = "urdf/hand.urdf"

world_id = pybullet.loadURDF("plane.urdf", [0, 0, 0])
robot_id = sim.loadURDF(urdf_path, basePosition=[0.0, 0.0, 0.0], 
                        useFixedBase=True,
                        flags=pybullet.URDF_USE_INERTIA_FROM_FILE)

full_urdf_path = pybullet_data.getDataPath()+'/'+urdf_path
print("full_urdf_path = ", full_urdf_path)

def getCurrentJointPosVel():
    cur_joint_states = sim.getJointStates(robot_id, controlled_joints)
    cur_joint_pos = [cur_joint_states[i][0] for i in range(n_dofs)]
    cur_joint_vel = [cur_joint_states[i][1] for i in range(n_dofs)]
    return cur_joint_pos, cur_joint_vel

def getCurrentEEVel():
    ee_state = sim.getLinkState(robot_id, n_dofs-1,
    	                        computeLinkVelocity=True, 
                                computeForwardKinematics=True)
    ee_lin_vel = np.array(ee_state[23]) # worldLinkLinearVelocity
    ee_ang_vel = np.array(ee_state[24]) # worldLinkAngularVelocity
    return ee_lin_vel, ee_ang_vel

def computeGravityCompensationControlPolicy():
    [cur_joint_pos, _] = getCurrentJointPosVel()
    grav_comp_torque = sim.calculateInverseDynamics(robot_id, cur_joint_pos, 
                                                    [0] * n_dofs, 
                                                    [0] * n_dofs)
    return np.array(grav_comp_torque)

def computeRBDInertiaMatrixAndNonLinearEffects(joint_pos, joint_vel):
    rbd_inertia_matrix = sim.calculateMassMatrix(robot_id, list(joint_pos))
    rbd_non_linear_effects = sim.calculateInverseDynamics(robot_id, joint_pos, joint_vel, [0.0] * 24)
    return np.asarray(rbd_inertia_matrix), np.asarray(rbd_non_linear_effects)

def computeForwardDynamics(joint_pos, joint_vel, torque, is_using_damping):
     [rbd_inertia_matrix, rbd_non_linear_effects
      ] = computeRBDInertiaMatrixAndNonLinearEffects(joint_pos, joint_vel)

     temp = torque - rbd_non_linear_effects
     
     if is_using_damping:
         damping_const = 0.5  # from URDF
         temp -= damping_const * np.array(joint_vel)
     qdd = list(np.matmul(np.linalg.inv(rbd_inertia_matrix), (temp).reshape(24)))
     return qdd

# activating torque control
sim.setJointMotorControlArray(
            bodyIndex=robot_id,
            jointIndices=controlled_joints,
            controlMode=pybullet.VELOCITY_CONTROL,
            forces=np.zeros(n_dofs))

prev_qd = [0.0] * n_dofs

time_count = 0

applied_torque = None

while True:
    [q, qd] = getCurrentJointPosVel()
    qdd = list((np.array(qd) - np.array(prev_qd))/dt)

    print("time_count = ", time_count)
    print("q   = ", np.array(q))
    print("qd  = ", np.array(qd))
    print("qdd = ", np.array(qdd))
    print('')

    if time_count > 2:
        assert(np.allclose(np.array(qd), (np.array(q) - np.array(prev_q))/dt, atol=1e-6))
    
    bullet_tau = np.array(sim.calculateInverseDynamics(robot_id, q, qd, qdd))

    grav_comp_torque = computeGravityCompensationControlPolicy()
    applied_torque = grav_comp_torque+addtnl_torque
    sim.setJointMotorControlArray(
            bodyIndex=robot_id,
            jointIndices=controlled_joints,
            controlMode=pybullet.TORQUE_CONTROL,
            forces=applied_torque)

    computed_qdd_wo_damping = computeForwardDynamics(q, qd, applied_torque, False)
    print('computed_qdd_wo_damping = ', computed_qdd_wo_damping)
    computed_qdd_w_damping = computeForwardDynamics(q, qd, applied_torque, True)
    print('computed_qdd_w_damping = ', computed_qdd_w_damping)

    prev_q = copy.deepcopy(q)
    prev_qd = copy.deepcopy(qd)
    time_count += 1
    
    sim.stepSimulation()
    time.sleep(dt)
