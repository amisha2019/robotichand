# q0 = initial position
# v0 = initial velocity
# q1 = final position
# v1 = final velocity
# t0 = initial time
# tf = final time


import pybullet as bullet
import time
import numpy as np
import json

q0 = [0]*15
v0 = [0]*15
with open('ikpos.txt') as json_file:
  q1 = json.load(json_file)

with open('ikvel.txt') as json_file:
  v1 = json.load(json_file)

#v1 = [0]*15
t0 = 0
tf = 5.0

#q0,v0,q1,v1,t0,tf = map(float,input('Initial Data (enter space separated) = [q0,v0,q1,v1,t0,tf] = ').split())

t = np.linspace(t0,tf,num = 100*int((tf-t0)))
tsize = 100*int((tf-t0))
c = np.ones(tsize) #1 X tsize

M = np.array([[1, t0, t0*t0, t0*t0*t0], 
              [0,1,2*t0,3*t0*t0],
              [1, tf, tf*tf, tf*tf*tf], 
              [0,1,2*tf,3*tf*tf]]) # 4 X 4

b = np.array([q0, v0, q1, v1])#should be 4 X 15
a = (np.transpose(M)) @ b #should be 4 X 15

qd = np.reshape(a[0],[15,1])*c + np.reshape(a[1],[15,1])*t + np.reshape(a[2],[15,1])*t*t + np.reshape(a[3],[15,1])*t*t*t #should be 15 X tsize
vd = np.reshape(a[1],[15,1])*c + 2*np.reshape(a[2],[15,1])*t + 3*np.reshape(a[3],[15,1])*t*t #should be 15 X tsize
ad = 2*np.reshape(a[2],[15,1])*c + 6*np.reshape(a[3],[15,1])*t #should be 15 X tsize


with open('desPos.txt', 'w') as outfile:
    json.dump(qd.tolist(),outfile)
with open('desVel.txt', 'w') as outfile:
    json.dump(vd.tolist(),outfile)
with open('desAcc.txt', 'w') as outfile:
    json.dump(ad.tolist(),outfile)

print(qd)
print(vd)
print(ad)