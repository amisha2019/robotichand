import pybullet as bullet
plot = True
import time

if (plot):
  import matplotlib.pyplot as plt
import math
verbose = False
# Parameters:
robot_base = [0., 0., 0.]
robot_orientation = [0., 0., 0., 1.]
delta_t = 0.001
# Initialize Bullet Simulator
id_simulator = bullet.connect(bullet.GUI)  # or bullet.DIRECT for non-graphical version
bullet.setTimeStep(delta_t)

cyaw=0
cpitch=-35
cdist=4.0
bullet.resetDebugVisualizerCamera( cameraDistance=cdist, cameraYaw=cyaw, cameraPitch=cpitch, cameraTargetPosition=(0.0, -0.35, 0.20))


bullet.setGravity(0,0,-0.32)
id_revolute_joints = [0,1,2,3,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
plane = bullet.loadURDF("urdf/plane.urdf")
#boxIDPick = bullet.loadURDF("urdf/Cylinder.urdf",[0,-1.55,0.85],bullet.getQuaternionFromEuler([1.57075,1.57075,1.57075]))
id_robot = bullet.loadURDF("urdf/hand.urdf",
                             robot_base,
                             robot_orientation,
                             globalScaling=0.50,
                             useFixedBase=True)


tstart = time.time()
while (1):
  bullet.stepSimulation()
  time.sleep(0.01)
  if(time.time() > tstart+3):
    bullet.setGravity(0,0,0)